package calculus

// Integral of a function f(x) between [a, b] is the
// area under the curve, represented by a sum of rectangular areas
func Integral(f func(float64) float64, a, b, dx float64) float64 {
	x := a
	sum := 0.0
	for x < b {
		sum += f(x) * dx
		x += dx
	}
	return sum
}

// Derivative of a function f(x) is a function g(x) for the
// given step dx
func Derivative(f func(float64) float64, dx float64) func(float64) float64 {
	return func(x float64) float64 {
		return (f(x+dx) - f(x)) / dx
	}
}
