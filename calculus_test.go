package calculus_test

import (
	"fmt"
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ataias/calculus"
)

func TestDerivative(t *testing.T) {
	f := func(x float64) float64 {
		return math.Sqrt(x)
	}

	dx := 0.0001
	expected := 0.5 * (1.0 / math.Sqrt(2))
	result := calculus.Derivative(f, dx)(2)
	epsilon := math.Abs(expected - result)
	errorMsg := fmt.Sprintf("Epsilon=%v greater than maximum=%v for derivative",
		epsilon,
		dx,
	)
	assert.InEpsilon(t, expected, result, dx, errorMsg)
}

func TestIntegral(t *testing.T) {
	f := func(x float64) float64 {
		return math.Sqrt(x)
	}

	dx := 0.0001
	g := func(x float64) float64 {
		return (2.0 / 3.0) * math.Pow(x, 1.5)
	}
	expected := g(4.0) - g(2.0)
	result := calculus.Integral(f, 2, 4, dx)
	epsilon := math.Abs(expected - result)
	errorMsg := fmt.Sprintf("Epsilon=%v greater than maximum=%v for integral",
		epsilon,
		dx,
	)
	assert.InEpsilon(t, expected, result, dx, errorMsg)
}
